def anagrams(word, words)
  word_array = word.split('').sort
  words.inject([]) { |anagrams, test| anagrams << test if word_array == test.split('').sort; anagrams }
end

puts anagrams"abba", ["aabb", "abcd", "bbaa", "dada"]
puts anagrams"racer", ["crazer", "carer", "racar", "caers", "racer"]
puts anagrams"laser", ["lazing", "lazy", "lacer"]
